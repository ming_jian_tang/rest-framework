## rest_framework框架

#### rest-framework框架

作用：前后端分离开发
```
模糊时可参考E:\pythonData\PythonVirtualenv\workspace\day07\hellodjango3中的app以及day08中的app
```
**使用方法演示**：

### urls.py

应用app下的urls.py内容：

​	rest_framework路由注册跟django中是有区别的

```python
# 注册路由地址资源
# 1.生成路由对象，可以用于管理路由地址
from app.views import ArticleView, TypeView, type_article, ArticleMsg, UserView

router = SimpleRouter()
# 2.注册资源
router.register('article', ArticleView)
router.register('type', TypeView)
# 注册用户资源
router.register('user', UserView)

urlpatterns = [
    path('type_view', type_article), # api_view装饰器
    path('article_msg', ArticleMsg.as_view()), # 类中的get方法
]
# 3.将自动生成的地址添加到django能识别的地址中
urlpatterns += router.urls
```

### settings.py

应用app下的settings.py的rest_framework配置**过滤类**、**renderer**、**分页**、**全局认证类**：

```python
REST_FRAMEWORK = {
    # 过滤类的配置
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    # 响应结果renderer配置
    'DEFAULT_RENDERER_CLASSES': (
        'utils.renderer.MyJsonRenderer',
    ),
    # 分页的配置
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    # 或者这样: 'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 3,
    # 全局认证类====>验证是否登录
    'DEFAULT_AUTHENTICATION_CLASSES': (
            'app.userauth.UserAuthentication',
    )
}
```

### model.py文件
创建一个文章模型Article、文章分类ArticleType、用户User模型

```python
class Article(models.Model):
    """文章模型"""
    title = models.CharField(max_length=255, null=False)
    desc = models.CharField(max_length=255, null=True)
    create_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 're_article'
        
class ArticleType(models.Model):
    """文章分类模型"""
    t_name = models.CharField(max_length=20, null=False, unique=True)

    class Meta:
        db_table = 'article_type'
        
class User(models.Model):
    """用户模型"""
    username = models.CharField(max_length=10, unique=True, null=False)
    password = models.CharField(max_length=255, null=False)

    class Meta:
        db_table = 'tb_user'
```
### view.py文件
创建视图类ArticleView和TypeView

在view.py中创建基于类的处理Article的视图类**ArticleView** 和处理TypeView的视图类TypeView

注意：ArticleView类的参数就对应了不同请求时会调用的方法

```
     方法               请求方式             路由的写法(id是表的主键pk的值)
CreateModelMixin    ---> POST           /app/article/
ListModelMixin      ---> GET            /app/article/
RetrieveModelMixin  ---> GET            /app/article/[id]/
DestroyModelMixin   ---> DELETE         /app/article/[id]/
UpdateModelMixin    ---> PUT/PATCH      /app/article/[id]/
```

```python
1.创建ArticleView
class ArticleView(viewsets.GenericViewSet,
                  mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.UpdateModelMixin):
	# 这个变量名不能改变，否则不能识别；queryset参数默认值为：路由注册的资源所对应的表的所有数据
    queryset = Article.objects.all()
    # # 序列化和字段校验的的功能，自动去调用ArticleSerializer中的方法
    serializer_class = ArticleSerializer
    
    # 过滤类(会自动调用)
    filter_class = ArticleFilter

    # =============1.重构get请求展示数据的方法============
    def list(self, request, *args, **kwargs):
        # get_queryset()方法默认返回Article.objects.all()的结果
        queryset = self.get_queryset()
        # 或者 ：queryset = self.queryset

        # get_serialiazer()方法默认返回ArticleSerializer,实现将queryset中的每一个对象序列化为dict
        serializer = self.get_serializer(queryset, many=True)
        # 或者： serializer = ArticleSerializer(queryset, many=True)
	    # 给前端返回序列化后的数据
        return Response(serializer.data)

    # ==========2.重构post请求提交的数据================
    def create(self, request, *args, **kwargs):
        # 1.接收post请求的参数
        data = request.data

        # 2.将获取的参数丢给ArticleSerializer进行字段校验(model中怎么定义的字段，他就会怎么校验)
        serializer = self.get_serializer(data=data)
        # 或 serializer = ArticleSerializer(data=data)

        # 3.is_valid()校验成功或失败
        result = serializer.is_valid()
        if result:
            # 成功就保存
            serializer.save()
            return Response(serializer.data)
        else:
            errors = serializer.errors
            return Response(errors)
        
	# ===========3.重构RetrieveModelMixin方法===============
    def retrieve(self, request, *args, **kwargs):
        # 1.获取对象
        instance = Article.objects.filter(pk=kwargs['pk']).first()
        # 或 instance = self.get_object()
        # 2.将拿到的对象序列化
        serializer = ArticleSerializer(instance)
        # 或 serializer = self.get_serializer(instance)
        # 3.将序列化的对象返回给前端
        return Response(serializer.data)

    # ===========4.重构DestroyModelMixin方法===========
    def destroy(self, request, *args, **kwargs):
        # 它本身是真的删除库中的数据，但我们可以通过重构，让它假删除
        # 拿到删除的对象
        instance = self.get_object()
        # 改变他的is_del属性为True
        instance.is_del = True
        # 删除操作完成，保存
        instance.save()

        return Response({'message': '删除成功'})

    # ===========5.重构UpdateModelMixin方法==============
    def update(self, request, *args, **kwargs):
        # 1.获取对象
        instance = self.get_object()
        # 2.获取要修改的参数
        data = request.data
        # 3.检验修改的参数
        serializer = self.get_serializer(instance, data=data)
        # 验证修改的参数是否合法，不合法抛出异常
        serializer.is_valid(raise_exception=True)
        # 4.合法就保存修改
        serializer.save()
        return Response({'message': '更新成功'})
知识点：
1.过滤类设置;--->filters.py文件
2.可以根据需要重构继承的类的方法
```
```python
2.创建TypeView
class TypeView(viewsets.GenericViewSet,
               mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.UpdateModelMixin):

    queryset = ArticleType.objects.all()

    serializer_class = ArticleTypeSerializer

    注意:在基于类的视图函数中还可以添加自己定义路由的方法
    """
    装饰器action，装饰器中的detail参数值为False时，定义的函数名称就将作为接口路由的一部分；
    如果想让其他请求也能触发这个地址可以在装饰器中加参数：methods=['请求方式',..]，但是一般只写一个
    @action(detail=False) 等于 @list_route()
    @action(detail=True)  等于 @detail_route()
    """
    # 1)定义/app/type/article/地址============
    @action(detail=False)
    def article(self, request):
        # 默认请求方式 ：GET
        # 现在请求地址 ： /app/type/article/
        """
            想要返回的json结构
            {
                {
                    id: '',
                    t_name: '',
                    articles:{},{}
                }
            }
        """
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)

    # 2)定义/app/type/[id]/articles/ 路由地址==============
    """
    detail=True时会自动生成/app/type/[id]/articles/ 这个地址，定义的函数名称就将作为接口路由的一部分，并且必须要传入主键参数pk；其中id值是ArticleType模型在数据库的主键值，表示只展示或删除主键值为id的articles
    """
    @action(detail=True)
    def articles(self, request, pk):
        # 请求方式：GET
        # 现在请求的地址： /app/type/[id]/articles/
        """
        返回的json的结构
        {
            {
                id: '',
                t_name: '',
                articles:{},
            }
        }
        """
        # 在这个路由下把东西展示出来
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        return Response(serializer.data)
知识点：
1.利用@action()装饰器自定义路由,设置default参数的值可以定义不同的路由
```
**除了用register去注册路由，我们还可以通过api_view装饰器去定义**

注意：注册定义资源和定义装饰器api_view的使用区别：

- 如果是注册资源，操作模型就是固定的；
- 如果是定义装饰器api_view和视图类中的get方法，操作模型没有固定,可以自定义，返回结果也自定义；

```python
3.api_view装饰器
{
        'code': '',
        'msg': '',
        'data': ''
    }
    
@api_view(['GET'])    # api_view必须给请求类型参数；GET,POST等
def type_article(request):
    """定义一个文章类型"""
    types = ArticleType.objects.all()
    articles = Article.objects.all()
    res = {
        'types': ArticleTypeSerializer(types, many=True).data,
        'articles': ArticleSerializer(articles, many=True).data,
        'msg': 'successful',
    }
    return Response(res)
```
**定义视图类中的get方法和上边定义装饰器api_view可以达到同样的效果**

```python
4.视图类的get方法
class ArticleMsg(APIView):
    """定义类中的get方法和上边定义装饰器api_view可以达到同样的效果"""
    def get(self, request, *args, **kwargs):
        types = ArticleType.objects.all()
        articles = Article.objects.all()
        res = {
            'types': ArticleTypeSerializer(types, many=True).data,
            'articles': ArticleSerializer(articles, many=True).data,
            'msg': 'successful',
        }
        return Response(res)
```
5.创建用户视图类:**UserView**

去验证serializer.py文件中的只校验字段的序列化类UserRegisterSerializer(serializers.Serializer)和UserLoginSerializer(serializers.Serializer)

```python
class UserView(viewsets.GenericViewSet,
               mixins.ListModelMixin):
    """注册、登录、注销"""

    queryset = User.objects.all()

    serializer_class = UserSerializer

    """
    1.注册地址: /app/user/register/
    在@action()中增加参数serializer_class参数可以重新指定这个函数需要调用的序列化方法
    """
    @action(detail=False, methods=['POST'], serializer_class=UserRegisterSerializer)
    def register(self, request):
        # 获取参数 --> 从request的data属性中拿参数
        data = request.data
        # 字段检验
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)

        serializer.save()  # 会自动调用
        return Response({'msg': '注册成功！'})

    # 2.注册地址: / app / user / login/
    @action(detail=False, methods=['POST'], serializer_class=UserLoginSerializer)
    def login(self, request):
        # 获取请求参数
        data = request.data
        # 字段校验
        serializer = self.get_serializer(data=data)
        # 验证是否合法,serializer.py
        serializer.is_valid(raise_exception=True)
        # 创建token和登录用户的关联关系及保存
        token = uuid.uuid4().hex
        user = User.objects.get(username=data.get('username'))
        # 保存token和用户关系到Redis
        cache.set(token, user.id, timeout=1000)
        res = {
            'token': token
        }

        return Response(res)
    
涉及知识点：
1.@action装饰器的使用(参数的设置)；
	在@action()中可以添加serializer_class参数指定调用新的序列化类，而不是上边视图类指定的序列化类；
例如：@action(detail=False, methods=['POST'], serializer_class=UserLoginSerializer)

2.保存用户登录身份标识token和用户的关系到Redis中；
	写入方式：cache.set(token, user.id, timeout=1000)
    timeout表示存活时间
    
3.只校验字段的序列化类UserRegisterSerializer、UserLoginSerializer的；设计、使用  
```
#### utils.py文件夹

1.如果我们想要自己处理返回给前端的数据结构：**重构renderer（）方法**

2.如果我们想自定义序列化时抛出的异常：**创建错误类，并继承APIException**

    步骤：在项目文件夹下创建utils.py文件夹，在再在里边创建__init.py、errors.py文件和 renderer.py文件
___

**1.renderer.py文件中renderer()方法的重构写法：**

- 重构了renderer()方法后，所有返回给前端的数据都会调用重构后的renderer()方法

```python
from rest_framework.renderers import JSONRenderer
class MyJsonRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        data中就是返回过来的数据，需要做的操作就是将data的值添加到code，msg，data中
            {
                'code': 0,
                'msg': '请求成功！',
                'data': data
            }
        注意：data中可能已经包含code、msg、data，处理时需要分情况
        """
        # msg动态生成
        try:
            msg = data.pop('msg')  # 取出来使用
        except:
            msg = '请求成功'
        # code动态生成
        try:
            code = data.pop('code')
        except:
            code = 0
        # data动态生成
        try:
            result = data['data']
        except:
            result = data
        res = {
                'code': code,
                'msg': msg,
                'data': result
        }
        # 如果要调用我们自己重构后的render方法，还需要到settings.py中区配置
        # 重新调用父类方法，将上边包装好的数据装换为json格式，返给前段
        return super().render(res)
```

**2.定制错误类型**

在errors.py文件中新建错误类

```python
from rest_framework.exceptions import APIException

# 错误都按照这个格式写
class ParamsException(APIException):
    """定义异常信息"""
    def __init__(self, msg):
        """跑出这个异常时，会自动调用这个init方法"""
        self.detail = msg
==================================================
知识点：
1.类名就是错误的类型名；
2.在抛出异常时，要传入抛出的异常的内容；
  例如：raise ParamsException({'code': 1005, 'msg': '用户不存在'})
```

![rest-framework框架请求](E:\pythonData\everyDayCourseData\Django\rest-framework框架请求.png)

### serializer.py文件

在app5应用文件夹下创建serializer.py文件，并在其中创建序列化类

**为什么要序列化？**

​	因为后端必须返回给前段json格式的数据，即将对象序列化为字典后的数据

**序列化类的作用**：

- 可以实现序列化：序列化的字段有fields来定义
- 实现字段校验：默认按照model中指定的Article模型中定义的字段约束条件进行校验;如果在这里重新定义了字段的条件，则按照这里定义约束条件去校验
- 除此还可以自定义重构字段验证方法**validate(self, attrs)**和自定义重构返回数据序列化后的数据的方法to_r**epresentation(self, instance)**，即可以**对返回的数据进行增、删、改、查**
```python
序列化的类的知识点：
  抛出错误(在utils.py文件夹下的errors.py去创建错误类)
  自定义验证字段的方法：validate()方法
  自定义返回数据的内容: to_representation()方法
```
**序列化的类分为两种**：

- 既能校验字段和序列化字段的类；继承**serializers.ModelSerializer**
- 只能检验字段的类；继承**serializers.Serializer**
- 两种类的继承的父类不一样

```python
1.创建把Article对象序列化的类ArticleSerializer

class ArticleSerializer(serializers.ModelSerializer):
    """序列化文章模型的序列化类"""
    title = serializers.CharField(required=True, max_length=20,
                                  error_messages={
                                      'required': 'title必填',
                                      'max_length': '最长为30字符'
                                  })
    class Meta:
        model = Article
        # __all__表示序列化所有字段，如果只序列化部分字段，就写成列表'['序列化的字段'...]'
        fields = '__all__' 
    """
    注意：在这里还可以定义序列化后数据的样式，增加、删除、修改数据；to_representation(self, instance)方法中instance参数表示需要序列化的对象参数
    """
    # 序列化时会自动调用
    def to_representation(self, instance):
        data = super().to_representation(instance)
        type = instance.type
        t_name = type.t_name
        data['type'] = t_name
        # data['type'] = ArticleType1Serializer(type).data
        return data
```

```python
2.创建把ArticleType对象序列化的类ArticleTypeSerializer

class ArticleTypeSerializer(serializers.ModelSerializer):
	"""序列化文章分类的类"""
    class Meta:
        model = ArticleType
        fields = '__all__'
        
    # 重构自定义验证，并抛出异常的方法 ---> validate()方法
    def validate(self, attrs):
        # attrs接收post请求参数数据
        t_name = attrs.get('t_name')
        if ArticleType.objects.filter(t_name=t_name).exists():
            # 抛出异常时必须在utils.py文件夹下的errors.py文件中定义错误类
            res = {'code': 1002, 'msg': '已存在此分类'}
            raise ParamsException(res)
        return attrs
    
    # 定义返回的数据序列化后的样式---> to_representation()方法
    def to_representation(self, instance):
        # 该方法就是进行序列化的方法; 对象 ---> dict
        # 先继承之前的方法
        data = super().to_representation(instance)

        # 返回给前端的数据一定是json数据
        articles = instance.article_set.all()
        # 将文章数据加入到返回的data中
        data['articles'] = ArticleSerializer(articles, many=True).data
        return data
```
```python
3.序列化用户、只校验用户登录、用户注册的类

class UserSerializer(serializers.ModelSerializer):
	"""序列化、校验用户"""
    class Meta:
        model = User
        fields = '__all__'

# 只校验注册字段
class UserRegisterSerializer(serializers.Serializer):
    """继承Serializer，只实现字段校验功能"""
    username = serializers.CharField(required=True, max_length=15, min_length=5)
    password = serializers.CharField(required=True, max_length=20, min_length=8)
    repassword = serializers.CharField(required=True, max_length=20, min_length=8)

    # 字段验证用户名是否已注册 --->和form表单验证类似，attrs相当于cleaned_data
    def validate(self, attrs):
        username = attrs.get('username')
        if User.objects.filter(username=username).exists():
            raise ParamsException({'code': 1003, 'msg': '账号已存在'})
        password = attrs.get('password')
        repassword =attrs.get('repassword')
        if password != repassword:
            raise ParamsException({'code': 1004, 'msg': '前后密码不一致'})
        return attrs

    # 会被save()自动调用
    def create(self, validated_data):
        """validated_data的数据是传过来的参数，即attrs中的数据 """
        username = validated_data.get('username')
        password = make_password(validated_data.get('password'))
        user = User.objects.create(username=username,
                            password=password)
        return user

# 只校验用户是否登录
class UserLoginSerializer(serializers.Serializer):
    """继承Serializer，只实现字段校验功能"""
    username = serializers.CharField(required=True, max_length=15, min_length=5)
    password = serializers.CharField(required=True, max_length=20, min_length=8)

    # 定义验证的方法
    def validate(self, attrs):
        username = attrs.get('username')
        user = User.objects.filter(username=username).first()
        if not user:
            raise ParamsException({'code': 1005, 'msg': '用户不存在'})
        password = attrs.get('password')
        if not check_password(password, user.password):
            raise ParamsException({'code': 1006, 'msg': '密码或用户名错误'})
        return attrs
知识点：
1.既能实现序列化、字段校验和只能实现字段校验的类继承的东西不一样
2.既能实现序列化、字段校验的类继承serializers.ModelSerializer；只能实现   字段校验的类继承serializers.Serializer
3.validate()字段校验方法的重构、create(self, validated_data)创建对象的方法
4.字段校验的参数attrs类似于form表单验证中的cleaned_data
```
### userauth.py

如果想要在全局的范围内验证用户是否登录，需要在应用app文件夹下创建userauth.py文件，并在settings.py文件中进行全局认证类的配置

```python
from django.core.cache import cache
from rest_framework.authentication import BaseAuthentication
from app.models import User
from utils.errors import ParamsException

class UserAuthentication(BaseAuthentication):
    """验证是否登录的认证类"""
    def authenticate(self, request):
        """GET请求接口参数在query_params中去取"""
        # 如果认证类是全局的，则过滤不需要验证登录的地址，如登录，注册
        path = request.path
        not_need_check = ['/app/user/register/', '/app/user/login/']
        if path not in not_need_check:
            token = request.query_params.get('token')
            if not token:
                raise ParamsException({'code': 1007, 'msg': '无法访问'})
            user_id = cache.get(token)
            if not user_id:
                raise ParamsException({'code': 1007, 'msg': '无法访问'})
            user = User.objects.get(pk=user_id)
            return user, token
知识点：
1.认证类的写法和排除不需要进行登录验证的路由
2.如果路由地址验证登录了，必须返回用户和token参数
```

### filters.py文件

在app5应用的文件夹下创建filters.py文件，对请求参数过滤

除此之外，还有去settings.py文件去进行过滤配置

**添加配置文件**：`REST_FRAMEWORK = {    # 过滤类的配置    'DEFAULT_FILTER_BACKENDS': (        'django_filters.rest_framework.DjangoFilterBackend',    )}`

```python
class ArticleFilter(django_filters.rest_framework.FilterSet):
    """实现过滤,多个条件时；如果接口参数有多个，多个参数都要满足"""
    # 左边的变量名是接口传来参数名的参数，变量名要与参数名一致
    # 接口定义的参数字段 = django_filters.CharFilter('模型中需要过滤的字段')
    title = django_filters.CharFilter('title')
    # 如果接口定义的参数字段与模型中过滤的字段一样，CharFilter中可以不写过滤字段
    # title = django_filters.CharFilter()
    """
    还可以模糊查询: contains / gt, gte / lt , lte
    title = django_filters.CharFilter(look_expr='icontains')
    """
    # 现在is_del参数字段值只能为True或False，要想接收其他参数，在后边需要定义一个方法，去处理这些值
    is_del = django_filters.CharFilter(method='filter_is_del')

    class Meat:
        model = Article
        fields = []
    # value接收参数字段的值
    def filter_is_del(self, queryset, name, value):
        if value == 'yes' or value == 'True':
            return queryset.filter(is_del=True)
        else:
            return queryset.filter(is_del=False)
```

### 不同请求，调用不同的方法

通过不同的请求方法时，就会自动完成对数据的CRUD(增、删、改、查)操作

**方法总结：**

```python
"""
接口有指定id参数时：
    instance = self.get_object() ---> 获取单个Article对象

instance = self.get_objects() ---> 获取所有的Article对象
instance = self.get_queryset() ---> 获取所有的Article对象
instance = Article.objects.all() ---> 获取所有的Article对象

serializer = self.get_serializer(instance) ---> 序列化获取的对象
serializer = ArticleSerializer(instance)  ---> 序列化获取的对象

serializer.data   ---> 返回前端的序列化后的数据
"""
```